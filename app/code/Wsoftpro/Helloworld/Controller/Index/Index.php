<?php
namespace Wsoftpro\HelloWorld\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		echo "Hello World";
		echo "<br>";
		echo "Xin chao tat ca cac ban";
		exit;
	}
}
?>


<?php

// namespace Wsoftpro\Helloworld\Controller\Index;

// use Magento\Framework\App\Action\Context;

// class Index extends \Magento\Framework\App\Action\Action
// {
//    protected $_resultPageFactory;

//    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
//    {
//        $this->_resultPageFactory = $resultPageFactory;
//        parent::__construct($context);
//    }

//    public function execute()
//    {
//        $resultPage = $this->_resultPageFactory->create();
//        return $resultPage;
//    }
// }
?>